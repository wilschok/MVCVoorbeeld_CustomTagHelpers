﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Razor.Runtime.TagHelpers;
using Microsoft.AspNetCore.Razor.TagHelpers;
using Microsoft.AspNetCore.Mvc.TagHelpers;
using Microsoft.AspNetCore.Mvc.ViewFeatures;

namespace LesDemo.TagHelpers
{
    [HtmlTargetElement("textarea")]
    public class CustomTextareaTagHelper : TextAreaTagHelper
    {
        public CustomTextareaTagHelper(IHtmlGenerator generator) : base(generator)
        {
        }

        [HtmlAttributeName("asp-for-width")]
        public int Cols { get; set; }

        [HtmlAttributeName("asp-for-height")]
        public int Rows { get; set; }

        [HtmlAttributeName("asp-is-disabled")]
        public bool IsDisabled { get; set; }

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            output.Attributes.Add("Rows", Rows);
            output.Attributes.Add("Cols", Cols);
            if (IsDisabled) {
                output.Attributes.Add("disabled", "disabled");
            }

            base.Process(context, output);
        }
    }
}
